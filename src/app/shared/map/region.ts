export interface IRegion {
  region_id: number;
  region_name: string;
  region_code: string;
}
