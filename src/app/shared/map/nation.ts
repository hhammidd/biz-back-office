export interface INation {
  nation_id: number;
  nation_name: string;
  nation_code;
}
