export interface IProvince {
  province_id: number;
  province_name: string;
  province_code: string;
}
