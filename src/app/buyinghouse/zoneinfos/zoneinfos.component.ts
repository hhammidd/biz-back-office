import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-zoneinfos',
  styles: [`
    agm-map {
      height: 350px;
    }
  `],
  templateUrl: './zoneinfos.component.html'
})
export class ZoneinfosComponent implements OnInit {

  lat: number = 51.678418;
  lng: number = 7.809007;

  constructor() {
  }

  ngOnInit() {
  }

}
